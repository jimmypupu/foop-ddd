package com.example.bryan.test;

import android.widget.ImageView;

/**
 * Created by Bryan on 2015/12/10.
 */
public class Dice {
    private int[] face = new int[6];
    private Monster monster;
    private Player owner;
    private int serialNumber;

    public Dice(String name, int ATK, int HP, int image_id, int face0, int face1, int face2, int face3, int face4, int face5, Player own, int attackType, int attackRange, int mobility, int attackTime, int serialNumber, boolean flyable)
    {
        this.owner = own;
        this.monster = new Monster(name, ATK, HP, image_id, own, attackType, attackRange, mobility, attackTime, flyable);
        this.face[0] = face0;
        this.face[1] = face1;
        this.face[2] = face2;
        this.face[3] = face3;
        this.face[4] = face4;
        this.face[5] = face5;
        this.serialNumber = serialNumber;

    }

    public int getFace(int i)
    {
        return  face[i];
    }
    public Monster getMonster()
    {
        return monster;
    }
    public int getserialNumber(){return  serialNumber;}
}
