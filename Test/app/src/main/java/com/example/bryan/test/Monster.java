package com.example.bryan.test;

import android.widget.ImageView;

/**
 * Created by Bryan on 2015/12/10.
 */
public class Monster {
    private String name;
    private int ATK;
    private int HP;
    private Player owner;
    private int image_id;
    private int attackType;
    // 0: can't attack, 1: cross, 2: radious
    private int attackRange;
    private boolean flyable;
    private int mobility;
    private int attackTime;
    private int MaxattackTime;

    public Monster(String name, int ATK, int HP, int image_id, Player player, int attackType, int attackRange, int mobility, int attackTime, boolean flyable)
    {
        this.owner = player;
        this.name = name;
        this.ATK = ATK;
        this.HP = HP;
        this.image_id = image_id;
        this.attackType = attackType;
        this.attackRange = attackRange;
        this.flyable = flyable;
        this.mobility = mobility;
        this.attackTime = attackTime;
        MaxattackTime = attackTime;
    }
    public Player getOwner(){return owner;}
    public int getId(){return image_id;}
    public void setId(int image_id){this.image_id = image_id;}
    public String getName()
    {
        return name;
    }
    public int getAttackType() {return attackType;}
    public int getAttackRange() {return attackRange;}
    public int getATK()
    {
        return ATK;
    }
    public int getHP()
    {
        return HP;
    }
    public void setATK(int ATK)
    {
        this.ATK = ATK;
    }
    public void setHP(int HP)
    {
        this.HP = HP;
    }
    public boolean getFlyable(){return this.flyable;}
    public int getMobility(){return mobility;}
    public void setattackTime(int attackTime){this.attackTime = attackTime;}
    public int getattackTime(){return attackTime;}
    public int getMaxattackTime(){return MaxattackTime;}
}
