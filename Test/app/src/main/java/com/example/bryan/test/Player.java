package com.example.bryan.test;

import android.widget.ImageView;

/**
 * Created by Bryan on 2015/12/8.
 */
public class Player {
    private int ID;
    private Dice[] dices = new Dice[12];
    private int[] used_dice = new int[12];
    private Dice[] using_dice = new Dice[3];
    private int[] point_pool = new int[6];
    private int tile_color;
    private int[] occupied_color = new int[2];
    private Monster dragon_heart;
    private int remainDice;

    public Player(int ID, int monsterID){
        this.ID = ID;
        dragon_heart = new Monster("Dragon_Heart", 0, 3, monsterID, this, 0, 0, 1, 1, false);
        initialDice();
        for(int i = 0; i < 6; i++)point_pool[i] = 0;
        remainDice = 12;
    }

    private void initialDice()
    {
        // 0 SUMMON
        // 1 MOVE
        // 2 ATK
        // 3 DEF
        // 4 MAGIC
        // 5 TRAP

        for(int i = 0; i < 4; i++){
            dices[i] = new Dice("Cannon", 5, 10, R.drawable.cannon, 0, 0, 0, 1, 1, 2, this, 1, 1, 1, 1, i, false);
            used_dice[i] = 1;
        }
        //tag test
        for(int i = 4; i < 8; i++){
            dices[i] = new Dice("Cannon", 5, 10, R.drawable.cannon, 0, 0, 0, 1, 1, 2, this, 2, 1, 1, 1, i, true);
            used_dice[i] = 1;
        }
        for(int i = 8; i < 12; i++){
            dices[i] = new Dice("Cannon", 5, 10, R.drawable.cannon, 0, 0, 0, 1, 1, 2, this, 0, 1, 1, 1, i, false);
            used_dice[i] = 1;
        }
    }

    public Dice[] throwDice()
    {
        int random, randoms[] = new int[]{-1,-1,-1}, check;
        for(int i = 0;i < 3;i++)
        {
            check = 0;
            while(check == 0)
            {
                random = (int) (Math.random() * 12);
                if (random != randoms[0] && random != randoms[1] && random != randoms[2])
                {
                    if (used_dice[random] == 1)
                    {
                        randoms[i] = random;
                        using_dice[i] = dices[random];
                        check = 1;
                    }
                }
            }
        }
        return using_dice;
    }

    public void clearPool(){
        point_pool[0] = 0;
    }

    public void addPool(int num){
        point_pool[num]++;
    }

    public int getPool(int type){
        return point_pool[type];
    }

    public boolean takePool(int type, int num){
        if(point_pool[type] - num >= 0){
            point_pool[type] -= num;
            return true;
        }
        else return false;
    }

    public Dice getDice(int index)
    {
        return dices[index];
    }

    public void getBack(int index)
    {
        used_dice[index] = 0;
    }

    public void setTile(int color, int color2, int color3){
        tile_color = color;
        occupied_color[0] = color2;
        occupied_color[1] = color3;
    }
    public int getTile(){return tile_color;}
    public int getOccupiedTile(int tile_owner){
        if(tile_owner == this.getID()) return occupied_color[0];
        else return occupied_color[1];
    }
    public Monster getDragon_heart()
    {
        return dragon_heart;
    }

    public int getID()
    {
        return ID;
    }
    public void setRemainDice(int remainDice){this.remainDice = remainDice;}
    public int getRemainDice(){return remainDice;}
}
