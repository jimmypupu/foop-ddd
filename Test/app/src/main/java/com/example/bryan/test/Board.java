package com.example.bryan.test;

import android.widget.ImageView;

/**
 * Created by jimmy on 2015/12/10.
 */
public class Board {
    public ImageView boardView;
    public Player owner;
    public Monster monster;

    public Board(ImageView view){
        owner = null;
        monster = null;
        boardView = view;
    }
}
