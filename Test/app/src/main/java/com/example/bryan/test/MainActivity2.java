package com.example.bryan.test;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;
import android.graphics.drawable.BitmapDrawable;

public class MainActivity2 extends ActionBarActivity {

    public Board[][] board = new Board[19][15];
    public Button[] buttons = new Button[4];
    public Dice[] tempDice;
    public int using;
    public TextView[] diceData = new TextView[2];
    public Player[] players = new Player[2];
    public int players_turn = 0;
    public int process = 1;
    public ImageButton[] select = new ImageButton[11];
    public GridLayout select_layout;
    public GridLayout select_layout2;
    public LinearLayout dice_select_layout;
    public ImageView[] dice_image = new ImageView[3];
    public ImageView[][] dice_tag = new ImageView[3][2];
    public TextView[] dice_info = new TextView[3];
    public int dice_faces[] = new int[3];
    public ImageView[] select_layout_print = new ImageView[8];

    public ClipData tmp2;
    public ImageView tmp, monsterDragView;
    private static final int DRAG_SURE_TIME = 1000;
    private int dragornot = 0;
    public int[][][][] open_index = new int[11][8][6][2];
    public int[][] current_index = new int[6][2];
    public int index1, index2;
    private void set_dice_open_index(){
        int i, j;
        for(i = 0; i < 11; i++)
            for(j = 0; j < 6; j++){
                open_index[i][j][0][0] = 0;
                open_index[i][j][0][1] = 0;
            }
        //1-1
        open_index[0][0][1][0] = -1;
        open_index[0][0][1][1] = 0;
        open_index[0][0][2][0] = 1;
        open_index[0][0][2][1] = 0;
        open_index[0][0][3][0] = 0;
        open_index[0][0][3][1] = 1;
        open_index[0][0][4][0] = 0;
        open_index[0][0][4][1] = 2;
        open_index[0][0][5][0] = 0;
        open_index[0][0][5][1] = 3;
        //1-2
        open_index[0][1][1][0] = -1;
        open_index[0][1][1][1] = 0;
        open_index[0][1][2][0] = -2;
        open_index[0][1][2][1] = 0;
        open_index[0][1][3][0] = -3;
        open_index[0][1][3][1] = 0;
        open_index[0][1][4][0] = 0;
        open_index[0][1][4][1] = 1;
        open_index[0][1][5][0] = 0;
        open_index[0][1][5][1] = -1;
        //1-3
        open_index[0][2][1][0] = 1;
        open_index[0][2][1][1] = 0;
        open_index[0][2][2][0] = 2;
        open_index[0][2][2][1] = 0;
        open_index[0][2][3][0] = 3;
        open_index[0][2][3][1] = 0;
        open_index[0][2][4][0] = 0;
        open_index[0][2][4][1] = -1;
        open_index[0][2][5][0] = 0;
        open_index[0][2][5][1] = 1;
        //1-4
        open_index[0][3][1][0] = 0;
        open_index[0][3][1][1] = -1;
        open_index[0][3][2][0] = 0;
        open_index[0][3][2][1] = -2;
        open_index[0][3][3][0] = 0;
        open_index[0][3][3][1] = -3;
        open_index[0][3][4][0] = -1;
        open_index[0][3][4][1] = 0;
        open_index[0][3][5][0] = 1;
        open_index[0][3][5][1] = 0;
        //2-1
        open_index[1][0][1][0] = -1;
        open_index[1][0][1][1] = -1;
        open_index[1][0][2][0] = 0;
        open_index[1][0][2][1] = -1;
        open_index[1][0][3][0] = 0;
        open_index[1][0][3][1] = 1;
        open_index[1][0][4][0] = 0;
        open_index[1][0][4][1] = 2;
        open_index[1][0][5][0] = 1;
        open_index[1][0][5][1] = 0;
        //2-2
        open_index[1][1][1][0] = 0;
        open_index[1][1][1][1] = 1;
        open_index[1][1][2][0] = -1;
        open_index[1][1][2][1] = 0;
        open_index[1][1][3][0] = -2;
        open_index[1][1][3][1] = 0;
        open_index[1][1][4][0] = 1;
        open_index[1][1][4][1] = 0;
        open_index[1][1][5][0] = 1;
        open_index[1][1][5][1] = -1;
        //2-3
        open_index[1][2][1][0] = 1;
        open_index[1][2][1][1] = 0;
        open_index[1][2][2][0] = -1;
        open_index[1][2][2][1] = -1;
        open_index[1][2][3][0] = 0;
        open_index[1][2][3][1] = -1;
        open_index[1][2][4][0] = 0;
        open_index[1][2][4][1] = -2;
        open_index[1][2][5][0] = 0;
        open_index[1][2][5][1] = -3;
        //2-4
        open_index[1][3][1][0] = 0;
        open_index[1][3][1][1] = -1;
        open_index[1][3][2][0] = -1;
        open_index[1][3][2][1] = 0;
        open_index[1][3][3][0] = -1;
        open_index[1][3][3][1] = 1;
        open_index[1][3][4][0] = 1;
        open_index[1][3][4][1] = 0;
        open_index[1][3][5][0] = 2;
        open_index[1][3][5][1] = 0;
        //2-5
        open_index[1][4][1][0] = 1;
        open_index[1][4][1][1] = -1;
        open_index[1][4][2][0] = -1;
        open_index[1][4][2][1] = 0;
        open_index[1][4][3][0] = 0;
        open_index[1][4][3][1] = -1;
        open_index[1][4][4][0] = 0;
        open_index[1][4][4][1] = 1;
        open_index[1][4][5][0] = 0;
        open_index[1][4][5][1] = 2;
        //2-6
        open_index[1][5][1][0] = 0;
        open_index[1][5][1][1] = -1;
        open_index[1][5][2][0] = -1;
        open_index[1][5][2][1] = 0;
        open_index[1][5][3][0] = -2;
        open_index[1][5][3][1] = 0;
        open_index[1][5][4][0] = 1;
        open_index[1][5][4][1] = 0;
        open_index[1][5][5][0] = 1;
        open_index[1][5][5][1] = 1;
        //2-7
        open_index[1][6][1][0] = 0;
        open_index[1][6][1][1] = -1;
        open_index[1][6][2][0] = 0;
        open_index[1][6][2][1] = -2;
        open_index[1][6][3][0] = 0;
        open_index[1][6][3][1] = 1;
        open_index[1][6][4][0] = 1;
        open_index[1][6][4][1] = 0;
        open_index[1][6][5][0] = -1;
        open_index[1][6][5][1] = 1;
        //2-8
        open_index[1][7][1][0] = 1;
        open_index[1][7][1][1] = 0;
        open_index[1][7][2][0] = 2;
        open_index[1][7][2][1] = 0;
        open_index[1][7][3][0] = -1;
        open_index[1][7][3][1] = 0;
        open_index[1][7][4][0] = 0;
        open_index[1][7][4][1] = 1;
        open_index[1][7][5][0] = -1;
        open_index[1][7][5][1] = -1;
        //3-1
        open_index[2][0][1][0] = 0;
        open_index[2][0][1][1] = -1;
        open_index[2][0][2][0] = 0;
        open_index[2][0][2][1] = -2;
        open_index[2][0][3][0] = -1;
        open_index[2][0][3][1] = -2;
        open_index[2][0][4][0] = 1;
        open_index[2][0][4][1] = 0;
        open_index[2][0][5][0] = 0;
        open_index[2][0][5][1] = 1;
        //3-2
        open_index[2][1][1][0] = -1;
        open_index[2][1][1][1] = 0;
        open_index[2][1][2][0] = 0;
        open_index[2][1][2][1] = 1;
        open_index[2][1][3][0] = 1;
        open_index[2][1][3][1] = 0;
        open_index[2][1][4][0] = 2;
        open_index[2][1][4][1] = 0;
        open_index[2][1][5][0] = 2;
        open_index[2][1][5][1] = -1;
        //3-3
        open_index[2][2][1][0] = -1;
        open_index[2][2][1][1] = 0;
        open_index[2][2][2][0] = 0;
        open_index[2][2][2][1] = -1;
        open_index[2][2][3][0] = 0;
        open_index[2][2][3][1] = 1;
        open_index[2][2][4][0] = 0;
        open_index[2][2][4][1] = 2;
        open_index[2][2][5][0] = 1;
        open_index[2][2][5][1] = 2;
        //3-4
        open_index[2][3][1][0] = -1;
        open_index[2][3][1][1] = 0;
        open_index[2][3][2][0] = -2;
        open_index[2][3][2][1] = 0;
        open_index[2][3][3][0] = 1;
        open_index[2][3][3][1] = 0;
        open_index[2][3][4][0] = 0;
        open_index[2][3][4][1] = -1;
        open_index[2][3][5][0] = -2;
        open_index[2][3][5][1] = 1;
        //3-5
        open_index[2][4][1][0] = 0;
        open_index[2][4][1][1] = -1;
        open_index[2][4][2][0] = 0;
        open_index[2][4][2][1] = -2;
        open_index[2][4][3][0] = 0;
        open_index[2][4][3][1] = 1;
        open_index[2][4][4][0] = -1;
        open_index[2][4][4][1] = 0;
        open_index[2][4][5][0] = 1;
        open_index[2][4][5][1] = -2;
        //3-6
        open_index[2][5][1][0] = 1;
        open_index[2][5][1][1] = 0;
        open_index[2][5][2][0] = 2;
        open_index[2][5][2][1] = 0;
        open_index[2][5][3][0] = -1;
        open_index[2][5][3][1] = 0;
        open_index[2][5][4][0] = 0;
        open_index[2][5][4][1] = -1;
        open_index[2][5][5][0] = 2;
        open_index[2][5][5][1] = 1;
        //3-7
        open_index[2][6][1][0] = 0;
        open_index[2][6][1][1] = -1;
        open_index[2][6][2][0] = 0;
        open_index[2][6][2][1] = 1;
        open_index[2][6][3][0] = 0;
        open_index[2][6][3][1] = 2;
        open_index[2][6][4][0] = 1;
        open_index[2][6][4][1] = 0;
        open_index[2][6][5][0] = -1;
        open_index[2][6][5][1] = 2;
        //3-8
        open_index[2][7][1][0] = 1;
        open_index[2][7][1][1] = 0;
        open_index[2][7][2][0] = -2;
        open_index[2][7][2][1] = 0;
        open_index[2][7][3][0] = -1;
        open_index[2][7][3][1] = 0;
        open_index[2][7][4][0] = 0;
        open_index[2][7][4][1] = 1;
        open_index[2][7][5][0] = -2;
        open_index[2][7][5][1] = -1;
        //4-1
        open_index[3][0][1][0] = -1;
        open_index[3][0][1][1] = 0;
        open_index[3][0][2][0] = 0;
        open_index[3][0][2][1] = 1;
        open_index[3][0][3][0] = 0;
        open_index[3][0][3][1] = 2;
        open_index[3][0][4][0] = 0;
        open_index[3][0][4][1] = 3;
        open_index[3][0][5][0] = 1;
        open_index[3][0][5][1] = 3;
        //4-2
        open_index[3][1][1][0] = -1;
        open_index[3][1][1][1] = 0;
        open_index[3][1][2][0] = 0;
        open_index[3][1][2][1] = -1;
        open_index[3][1][3][0] = -2;
        open_index[3][1][3][1] = 0;
        open_index[3][1][4][0] = -3;
        open_index[3][1][4][1] = 0;
        open_index[3][1][5][0] = -3;
        open_index[3][1][5][1] = 1;
        //4-3
        open_index[3][2][1][0] = -1;
        open_index[3][2][1][1] = 0;
        open_index[3][2][2][0] = 0;
        open_index[3][2][2][1] = 1;
        open_index[3][2][3][0] = 0;
        open_index[3][2][3][1] = 2;
        open_index[3][2][4][0] = 0;
        open_index[3][2][4][1] = 3;
        open_index[3][2][5][0] = 1;
        open_index[3][2][5][1] = 3;
        //4-4
        open_index[3][3][1][0] = -1;
        open_index[3][3][1][1] = 0;
        open_index[3][3][2][0] = -2;
        open_index[3][3][2][1] = 0;
        open_index[3][3][3][0] = -3;
        open_index[3][3][3][1] = 0;
        open_index[3][3][4][0] = 0;
        open_index[3][3][4][1] = -1;
        open_index[3][3][5][0] = -3;
        open_index[3][3][5][1] = 1;
        //4-5
        open_index[3][4][1][0] = 1;
        open_index[3][4][1][1] = 0;
        open_index[3][4][2][0] = 0;
        open_index[3][4][2][1] = 1;
        open_index[3][4][3][0] = 0;
        open_index[3][4][3][1] = 2;
        open_index[3][4][4][0] = 0;
        open_index[3][4][4][1] = 3;
        open_index[3][4][5][0] = -1;
        open_index[3][4][5][1] = 3;
        //4-6
        open_index[3][5][1][0] = -1;
        open_index[3][5][1][1] = 0;
        open_index[3][5][2][0] = -2;
        open_index[3][5][2][1] = 0;
        open_index[3][5][3][0] = -3;
        open_index[3][5][3][1] = 0;
        open_index[3][5][4][0] = 0;
        open_index[3][5][4][1] = 1;
        open_index[3][5][5][0] = -3;
        open_index[3][5][5][1] = -1;
        //4-7
        open_index[3][6][1][0] = 1;
        open_index[3][6][1][1] = 0;
        open_index[3][6][2][0] = 0;
        open_index[3][6][2][1] = 1;
        open_index[3][6][3][0] = 0;
        open_index[3][6][3][1] = 2;
        open_index[3][6][4][0] = 0;
        open_index[3][6][4][1] = 3;
        open_index[3][6][5][0] = -1;
        open_index[3][6][5][1] = 3;
        //4-8
        open_index[3][7][1][0] = -1;
        open_index[3][7][1][1] = 0;
        open_index[3][7][2][0] = -2;
        open_index[3][7][2][1] = 0;
        open_index[3][7][3][0] = -3;
        open_index[3][7][3][1] = 0;
        open_index[3][7][4][0] = 0;
        open_index[3][7][4][1] = 1;
        open_index[3][7][5][0] = -3;
        open_index[3][7][5][1] = -1;
        //5-1
        open_index[4][0][1][0] = 1;
        open_index[4][0][1][1] = 0;
        open_index[4][0][2][0] = -1;
        open_index[4][0][2][1] = 0;
        open_index[4][0][3][0] = 0;
        open_index[4][0][3][1] = 1;
        open_index[4][0][4][0] = 0;
        open_index[4][0][4][1] = 2;
        open_index[4][0][5][0] = 0;
        open_index[4][0][5][1] = -1;
        //5-2
        open_index[4][1][1][0] = 1;
        open_index[4][1][1][1] = 0;
        open_index[4][1][2][0] = -1;
        open_index[4][1][2][1] = 0;
        open_index[4][1][3][0] = 0;
        open_index[4][1][3][1] = 1;
        open_index[4][1][4][0] = 0;
        open_index[4][1][4][1] = -1;
        open_index[4][1][5][0] = -2;
        open_index[4][1][5][1] = 0;
        //5-3
        open_index[4][2][1][0] = 1;
        open_index[4][2][1][1] = 0;
        open_index[4][2][2][0] = -1;
        open_index[4][2][2][1] = 0;
        open_index[4][2][3][0] = 0;
        open_index[4][2][3][1] = 1;
        open_index[4][2][4][0] = 0;
        open_index[4][2][4][1] = -1;
        open_index[4][2][5][0] = 0;
        open_index[4][2][5][1] = -2;
        //5-4
        open_index[4][3][1][0] = -1;
        open_index[4][3][1][1] = 0;
        open_index[4][3][2][0] = 1;
        open_index[4][3][2][1] = 0;
        open_index[4][3][3][0] = 0;
        open_index[4][3][3][1] = 1;
        open_index[4][3][4][0] = 0;
        open_index[4][3][4][1] = -1;
        open_index[4][3][5][0] = 2;
        open_index[4][3][5][1] = 0;
        //6-1
        open_index[5][0][1][0] = -1;
        open_index[5][0][1][1] = 0;
        open_index[5][0][2][0] = 0;
        open_index[5][0][2][1] = -1;
        open_index[5][0][3][0] = 0;
        open_index[5][0][3][1] = 1;
        open_index[5][0][4][0] = 1;
        open_index[5][0][4][1] = 1;
        open_index[5][0][5][0] = 0;
        open_index[5][0][5][1] = 2;
        //6-2
        open_index[5][1][1][0] = -1;
        open_index[5][1][1][1] = 0;
        open_index[5][1][2][0] = 0;
        open_index[5][1][2][1] = -1;
        open_index[5][1][3][0] = 1;
        open_index[5][1][3][1] = 0;
        open_index[5][1][4][0] = -1;
        open_index[5][1][4][1] = 1;
        open_index[5][1][5][0] = -2;
        open_index[5][1][5][1] = 0;
        //6-3
        open_index[5][2][1][0] = 0;
        open_index[5][2][1][1] = -1;
        open_index[5][2][2][0] = 0;
        open_index[5][2][2][1] = 1;
        open_index[5][2][3][0] = 0;
        open_index[5][2][3][1] = 2;
        open_index[5][2][4][0] = -1;
        open_index[5][2][4][1] = 0;
        open_index[5][2][5][0] = 1;
        open_index[5][2][5][1] = 1;
        //6-4
        open_index[5][3][1][0] = -1;
        open_index[5][3][1][1] = 0;
        open_index[5][3][2][0] = 0;
        open_index[5][3][2][1] = -1;
        open_index[5][3][3][0] = 1;
        open_index[5][3][3][1] = 0;
        open_index[5][3][4][0] = -1;
        open_index[5][3][4][1] = 1;
        open_index[5][3][5][0] = -2;
        open_index[5][3][5][1] = 0;
        //6-5
        open_index[5][4][1][0] = 1;
        open_index[5][4][1][1] = 0;
        open_index[5][4][2][0] = -1;
        open_index[5][4][2][1] = 0;
        open_index[5][4][3][0] = 0;
        open_index[5][4][3][1] = -1;
        open_index[5][4][4][0] = 2;
        open_index[5][4][4][1] = 0;
        open_index[5][4][5][0] = 1;
        open_index[5][4][5][1] = 1;
        //6-6
        open_index[5][5][1][0] = 0;
        open_index[5][5][1][1] = -1;
        open_index[5][5][2][0] = 0;
        open_index[5][5][2][1] = 1;
        open_index[5][5][3][0] = 0;
        open_index[5][5][3][1] = 2;
        open_index[5][5][4][0] = 1;
        open_index[5][5][4][1] = 0;
        open_index[5][5][5][0] = -1;
        open_index[5][5][5][1] = 1;
        //6-7
        open_index[5][6][1][0] = 1;
        open_index[5][6][1][1] = 0;
        open_index[5][6][2][0] = -1;
        open_index[5][6][2][1] = 0;
        open_index[5][6][3][0] = 0;
        open_index[5][6][3][1] = -1;
        open_index[5][6][4][0] = 2;
        open_index[5][6][4][1] = 0;
        open_index[5][6][5][0] = 1;
        open_index[5][6][5][1] = 1;
        //6-8
        open_index[5][7][1][0] = 0;
        open_index[5][7][1][1] = -1;
        open_index[5][7][2][0] = 0;
        open_index[5][7][2][1] = 1;
        open_index[5][7][3][0] = 0;
        open_index[5][7][3][1] = 2;
        open_index[5][7][4][0] = 1;
        open_index[5][7][4][1] = 0;
        open_index[5][7][5][0] = -1;
        open_index[5][7][5][1] = 1;
        //7-1
        open_index[6][0][1][0] = 0;
        open_index[6][0][1][1] = -1;
        open_index[6][0][2][0] = 0;
        open_index[6][0][2][1] = -2;
        open_index[6][0][3][0] = -1;
        open_index[6][0][3][1] = -1;
        open_index[6][0][4][0] = 1;
        open_index[6][0][4][1] = 0;
        open_index[6][0][5][0] = 1;
        open_index[6][0][5][1] = 1;
        //7-2
        open_index[6][1][1][0] = 1;
        open_index[6][1][1][1] = 0;
        open_index[6][1][2][0] = 1;
        open_index[6][1][2][1] = -1;
        open_index[6][1][3][0] = 2;
        open_index[6][1][3][1] = 0;
        open_index[6][1][4][0] = 0;
        open_index[6][1][4][1] = 1;
        open_index[6][1][5][0] = -1;
        open_index[6][1][5][1] = 1;
        //7-3
        open_index[6][2][1][0] = -1;
        open_index[6][2][1][1] = 0;
        open_index[6][2][2][0] = -1;
        open_index[6][2][2][1] = -1;
        open_index[6][2][3][0] = 0;
        open_index[6][2][3][1] = 1;
        open_index[6][2][4][0] = 0;
        open_index[6][2][4][1] = 2;
        open_index[6][2][5][0] = 1;
        open_index[6][2][5][1] = 1;
        //7-4
        open_index[6][3][1][0] = -1;
        open_index[6][3][1][1] = 0;
        open_index[6][3][2][0] = 0;
        open_index[6][3][2][1] = -1;
        open_index[6][3][3][0] = 1;
        open_index[6][3][3][1] = -1;
        open_index[6][3][4][0] = -1;
        open_index[6][3][4][1] = 1;
        open_index[6][3][5][0] = -2;
        open_index[6][3][5][1] = 0;
        //7-5
        open_index[6][4][1][0] = 1;
        open_index[6][4][1][1] = -1;
        open_index[6][4][2][0] = 0;
        open_index[6][4][2][1] = -1;
        open_index[6][4][3][0] = 0;
        open_index[6][4][3][1] = -2;
        open_index[6][4][4][0] = -1;
        open_index[6][4][4][1] = 0;
        open_index[6][4][5][0] = -1;
        open_index[6][4][5][1] = 1;
        //7-6
        open_index[6][5][1][0] = 0;
        open_index[6][5][1][1] = -1;
        open_index[6][5][2][0] = -1;
        open_index[6][5][2][1] = -1;
        open_index[6][5][3][0] = 1;
        open_index[6][5][3][1] = 0;
        open_index[6][5][4][0] = 2;
        open_index[6][5][4][1] = 0;
        open_index[6][5][5][0] = 1;
        open_index[6][5][5][1] = 1;
        //7-7
        open_index[6][6][1][0] = 1;
        open_index[6][6][1][1] = 0;
        open_index[6][6][2][0] = 1;
        open_index[6][6][2][1] = -1;
        open_index[6][6][3][0] = 0;
        open_index[6][6][3][1] = 1;
        open_index[6][6][4][0] = 0;
        open_index[6][6][4][1] = 2;
        open_index[6][6][5][0] = -1;
        open_index[6][6][5][1] = 1;
        //7-8
        open_index[6][7][1][0] = -1;
        open_index[6][7][1][1] = 0;
        open_index[6][7][2][0] = -1;
        open_index[6][7][2][1] = -1;
        open_index[6][7][3][0] = -2;
        open_index[6][7][3][1] = 0;
        open_index[6][7][4][0] = 0;
        open_index[6][7][4][1] = 1;
        open_index[6][7][5][0] = 1;
        open_index[6][7][5][1] = 1;
        //8-1
        open_index[7][0][1][0] = 0;
        open_index[7][0][1][1] = 1;
        open_index[7][0][2][0] = 0;
        open_index[7][0][2][1] = 2;
        open_index[7][0][3][0] = -1;
        open_index[7][0][3][1] = 0;
        open_index[7][0][4][0] = 1;
        open_index[7][0][4][1] = 0;
        open_index[7][0][5][0] = 1;
        open_index[7][0][5][1] = -1;
        //8-2
        open_index[7][1][1][0] = -1;
        open_index[7][1][1][1] = 0;
        open_index[7][1][2][0] = -2;
        open_index[7][1][2][1] = 0;
        open_index[7][1][3][0] = 0;
        open_index[7][1][3][1] = -1;
        open_index[7][1][4][0] = 0;
        open_index[7][1][4][1] = 1;
        open_index[7][1][5][0] = 1;
        open_index[7][1][5][1] = 1;
        //8-3
        open_index[7][2][1][0] = -1;
        open_index[7][2][1][1] = 0;
        open_index[7][2][2][0] = 1;
        open_index[7][2][2][1] = 0;
        open_index[7][2][3][0] = 0;
        open_index[7][2][3][1] = -1;
        open_index[7][2][4][0] = 0;
        open_index[7][2][4][1] = -2;
        open_index[7][2][5][0] = -1;
        open_index[7][2][5][1] = 1;
        //8-4
        open_index[7][3][1][0] = -1;
        open_index[7][3][1][1] = -1;
        open_index[7][3][2][0] = 0;
        open_index[7][3][2][1] = -1;
        open_index[7][3][3][0] = 0;
        open_index[7][3][3][1] = 1;
        open_index[7][3][4][0] = 1;
        open_index[7][3][4][1] = 0;
        open_index[7][3][5][0] = 2;
        open_index[7][3][5][1] = 0;
        //8-5
        open_index[7][4][1][0] = -1;
        open_index[7][4][1][1] = -1;
        open_index[7][4][2][0] = -1;
        open_index[7][4][2][1] = 0;
        open_index[7][4][3][0] = 1;
        open_index[7][4][3][1] = 0;
        open_index[7][4][4][0] = 0;
        open_index[7][4][4][1] = 1;
        open_index[7][4][5][0] = 0;
        open_index[7][4][5][1] = 2;
        //8-6
        open_index[7][5][1][0] = -1;
        open_index[7][5][1][1] = 0;
        open_index[7][5][2][0] = -2;
        open_index[7][5][2][1] = 0;
        open_index[7][5][3][0] = 0;
        open_index[7][5][3][1] = 1;
        open_index[7][5][4][0] = 0;
        open_index[7][5][4][1] = -1;
        open_index[7][5][5][0] = 1;
        open_index[7][5][5][1] = -1;
        //8-7
        open_index[7][6][1][0] = 1;
        open_index[7][6][1][1] = 0;
        open_index[7][6][2][0] = -1;
        open_index[7][6][2][1] = 0;
        open_index[7][6][3][0] = 0;
        open_index[7][6][3][1] = -1;
        open_index[7][6][4][0] = 0;
        open_index[7][6][4][1] = -2;
        open_index[7][6][5][0] = 1;
        open_index[7][6][5][1] = 1;
        //8-8
        open_index[7][7][1][0] = 1;
        open_index[7][7][1][1] = 0;
        open_index[7][7][2][0] = 2;
        open_index[7][7][2][1] = 0;
        open_index[7][7][3][0] = 0;
        open_index[7][7][3][1] = -1;
        open_index[7][7][4][0] = 0;
        open_index[7][7][4][1] = 1;
        open_index[7][7][5][0] = -1;
        open_index[7][7][5][1] = 1;
        //9-1
        open_index[8][0][1][0] = 0;
        open_index[8][0][1][1] = -1;
        open_index[8][0][2][0] = 0;
        open_index[8][0][2][1] = -2;
        open_index[8][0][3][0] = 1;
        open_index[8][0][3][1] = 0;
        open_index[8][0][4][0] = 1;
        open_index[8][0][4][1] = 1;
        open_index[8][0][5][0] = 1;
        open_index[8][0][5][1] = 2;
        //9-2
        open_index[8][1][1][0] = 1;
        open_index[8][1][1][1] = 0;
        open_index[8][1][2][0] = 2;
        open_index[8][1][2][1] = 0;
        open_index[8][1][3][0] = 0;
        open_index[8][1][3][1] = 1;
        open_index[8][1][4][0] = -1;
        open_index[8][1][4][1] = 1;
        open_index[8][1][5][0] = -2;
        open_index[8][1][5][1] = 1;
        //9-3
        open_index[8][2][1][0] = 0;
        open_index[8][2][1][1] = -1;
        open_index[8][2][2][0] = 0;
        open_index[8][2][2][1] = -2;
        open_index[8][2][3][0] = 1;
        open_index[8][2][3][1] = 0;
        open_index[8][2][4][0] = 1;
        open_index[8][2][4][1] = 1;
        open_index[8][2][5][0] = 1;
        open_index[8][2][5][1] = 2;
        //9-4
        open_index[8][3][1][0] = 1;
        open_index[8][3][1][1] = 0;
        open_index[8][3][2][0] = 2;
        open_index[8][3][2][1] = 0;
        open_index[8][3][3][0] = 0;
        open_index[8][3][3][1] = 1;
        open_index[8][3][4][0] = -1;
        open_index[8][3][4][1] = 1;
        open_index[8][3][5][0] = -2;
        open_index[8][3][5][1] = 1;
        //9-5
        open_index[8][4][1][0] = 1;
        open_index[8][4][1][1] = 0;
        open_index[8][4][2][0] = 1;
        open_index[8][4][2][1] = -1;
        open_index[8][4][3][0] = 1;
        open_index[8][4][3][1] = -2;
        open_index[8][4][4][0] = 0;
        open_index[8][4][4][1] = 1;
        open_index[8][4][5][0] = 0;
        open_index[8][4][5][1] = 2;
        //9-6
        open_index[8][5][1][0] = -1;
        open_index[8][5][1][1] = 0;
        open_index[8][5][2][0] = -2;
        open_index[8][5][2][1] = 0;
        open_index[8][5][3][0] = 0;
        open_index[8][5][3][1] = 1;
        open_index[8][5][4][0] = 1;
        open_index[8][5][4][1] = 1;
        open_index[8][5][5][0] = 2;
        open_index[8][5][5][1] = 1;
        //9-7
        open_index[8][6][1][0] = 1;
        open_index[8][6][1][1] = 0;
        open_index[8][6][2][0] = 1;
        open_index[8][6][2][1] = -1;
        open_index[8][6][3][0] = 1;
        open_index[8][6][3][1] = -2;
        open_index[8][6][4][0] = 0;
        open_index[8][6][4][1] = 1;
        open_index[8][6][5][0] = 0;
        open_index[8][6][5][1] = 2;
        //9-8
        open_index[8][7][1][0] = -1;
        open_index[8][7][1][1] = 0;
        open_index[8][7][2][0] = -2;
        open_index[8][7][2][1] = 0;
        open_index[8][7][3][0] = 0;
        open_index[8][7][3][1] = 1;
        open_index[8][7][4][0] = 1;
        open_index[8][7][4][1] = 1;
        open_index[8][7][5][0] = 2;
        open_index[8][7][5][1] = 1;
        //10-1
        open_index[9][0][1][0] = -1;
        open_index[9][0][1][1] = 0;
        open_index[9][0][2][0] = -1;
        open_index[9][0][2][1] = -1;
        open_index[9][0][3][0] = 0;
        open_index[9][0][3][1] = 1;
        open_index[9][0][4][0] = 0;
        open_index[9][0][4][1] = 2;
        open_index[9][0][5][0] = 1;
        open_index[9][0][5][1] = 2;
        //10-2
        open_index[9][1][1][0] = 0;
        open_index[9][1][1][1] = -1;
        open_index[9][1][2][0] = 1;
        open_index[9][1][2][1] = -1;
        open_index[9][1][3][0] = -1;
        open_index[9][1][3][1] = 0;
        open_index[9][1][4][0] = -2;
        open_index[9][1][4][1] = 0;
        open_index[9][1][5][0] = -2;
        open_index[9][1][5][1] = 1;
        //10-3
        open_index[9][2][1][0] = 0;
        open_index[9][2][1][1] = -1;
        open_index[9][2][2][0] = 0;
        open_index[9][2][2][1] = -2;
        open_index[9][2][3][0] = -1;
        open_index[9][2][3][1] = -2;
        open_index[9][2][4][0] = 1;
        open_index[9][2][4][1] = 0;
        open_index[9][2][5][0] = 1;
        open_index[9][2][5][1] = 1;
        //10-4
        open_index[9][3][1][0] = 1;
        open_index[9][3][1][1] = 0;
        open_index[9][3][2][0] = 2;
        open_index[9][3][2][1] = 0;
        open_index[9][3][3][0] = 2;
        open_index[9][3][3][1] = -1;
        open_index[9][3][4][0] = 0;
        open_index[9][3][4][1] = 1;
        open_index[9][3][5][0] = -1;
        open_index[9][3][5][1] = 1;
        //10-5
        open_index[9][4][1][0] = 1;
        open_index[9][4][1][1] = 0;
        open_index[9][4][2][0] = 1;
        open_index[9][4][2][1] = -1;
        open_index[9][4][3][0] = 0;
        open_index[9][4][3][1] = 1;
        open_index[9][4][4][0] = 0;
        open_index[9][4][4][1] = 2;
        open_index[9][4][5][0] = -1;
        open_index[9][4][5][1] = 2;
        //10-6
        open_index[9][5][1][0] = -1;
        open_index[9][5][1][1] = 0;
        open_index[9][5][2][0] = -2;
        open_index[9][5][2][1] = 0;
        open_index[9][5][3][0] = -2;
        open_index[9][5][3][1] = -1;
        open_index[9][5][4][0] = 0;
        open_index[9][5][4][1] = 1;
        open_index[9][5][5][0] = 1;
        open_index[9][5][5][1] = 1;
        //10-7
        open_index[9][6][1][0] = 0;
        open_index[9][6][1][1] = -1;
        open_index[9][6][2][0] = 0;
        open_index[9][6][2][1] = -2;
        open_index[9][6][3][0] = 1;
        open_index[9][6][3][1] = -2;
        open_index[9][6][4][0] = -1;
        open_index[9][6][4][1] = 0;
        open_index[9][6][5][0] = -1;
        open_index[9][6][5][1] = 1;
        //10-8
        open_index[9][7][1][0] = 1;
        open_index[9][7][1][1] = 0;
        open_index[9][7][2][0] = 2;
        open_index[9][7][2][1] = 0;
        open_index[9][7][3][0] = 2;
        open_index[9][7][3][1] = 1;
        open_index[9][7][4][0] = 0;
        open_index[9][7][4][1] = -1;
        open_index[9][7][5][0] = -1;
        open_index[9][7][5][1] = -1;
        //11-1
        open_index[10][0][1][0] = -1;
        open_index[10][0][1][1] = 0;
        open_index[10][0][2][0] = -1;
        open_index[10][0][2][1] = -1;
        open_index[10][0][3][0] = 0;
        open_index[10][0][3][1] = 1;
        open_index[10][0][4][0] = 1;
        open_index[10][0][4][1] = 1;
        open_index[10][0][5][0] = 1;
        open_index[10][0][5][1] = 2;
        //11-2
        open_index[10][1][1][0] = 0;
        open_index[10][1][1][1] = -1;
        open_index[10][1][2][0] = 1;
        open_index[10][1][2][1] = -1;
        open_index[10][1][3][0] = -1;
        open_index[10][1][3][1] = 0;
        open_index[10][1][4][0] = -1;
        open_index[10][1][4][1] = 1;
        open_index[10][1][5][0] = -2;
        open_index[10][1][5][1] = 1;
        //11-3
        open_index[10][2][1][0] = 0;
        open_index[10][2][1][1] = -1;
        open_index[10][2][2][0] = 1;
        open_index[10][2][2][1] = -1;
        open_index[10][2][3][0] = 1;
        open_index[10][2][3][1] = -2;
        open_index[10][2][4][0] = -1;
        open_index[10][2][4][1] = 0;
        open_index[10][2][5][0] = -1;
        open_index[10][2][5][1] = 1;
        //11-4
        open_index[10][3][1][0] = -1;
        open_index[10][3][1][1] = 0;
        open_index[10][3][2][0] = -1;
        open_index[10][3][2][1] = -1;
        open_index[10][3][3][0] = -2;
        open_index[10][3][3][1] = -1;
        open_index[10][3][4][0] = 0;
        open_index[10][3][4][1] = 1;
        open_index[10][3][5][0] = 1;
        open_index[10][3][5][1] = 1;
    }
    public Thread multiListenerThread;
    public int multiListenerWaiting;
    private Monster summoningMonster;

    private Board[] bfsQueue = new Board[285];
    private int[][] bfsVisit = new int[19][15];
    private int queueTop, queuePointer, run_or_not;
    private Board[] history = new Board[2];
    private int redeem;

    public float scale;
    public int pixels;

    public int temp_monId;


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    private void setToOriginColor(){
        Board recoveringBoard;
        for(int i = 0;i < queueTop; i++){
            recoveringBoard = bfsQueue[i];
            if(recoveringBoard.monster == null)recoveringBoard.boardView.setBackgroundResource(recoveringBoard.owner.getTile());
            else{
                recoveringBoard.boardView.setImageResource(recoveringBoard.monster.getId());
                recoveringBoard.boardView.setBackgroundResource(recoveringBoard.monster.getOwner().getOccupiedTile(recoveringBoard.owner.getID()));
            }
        }
    }

    private void checkMoveable(int move, boolean flyable){
        Board expandedBoard;
        int x,y;
        for(; queuePointer < queueTop; queuePointer++){
            expandedBoard = bfsQueue[queuePointer];
            x = expandedBoard.boardView.getId()%15;
            y = expandedBoard.boardView.getId()/15;
            Log.i("Log", "in BFS, node= "+y+" "+x);
            expandedBoard.boardView.setBackgroundResource(R.drawable.white);
            if(expandedBoard.owner != null && expandedBoard.monster != null && expandedBoard.monster.getOwner() != players[players_turn]) {
                expandedBoard.boardView.setBackgroundResource(R.drawable.invalid_tile);
                if(!flyable) {
                    bfsVisit[y][x] = -1;
                    continue;
                }
            }
            if(expandedBoard.owner != null && expandedBoard.monster != null && expandedBoard.monster.getOwner() == players[players_turn])
                expandedBoard.boardView.setBackgroundResource(R.drawable.black_tile);
            if(bfsVisit[y][x] == move){
                continue;
            }
            if(x < 14 && board[y][x+1].owner != null){
                if(bfsVisit[y][x+1] == -1) {
                    bfsVisit[y][x+1] = bfsVisit[y][x] + 1;
                    bfsQueue[queueTop] = board[y][x+1];
                    queueTop++;
                }
            }
            if(x > 0 && board[y][x-1].owner != null){
                if(bfsVisit[y][x-1] == -1) {
                    bfsVisit[y][x-1] = bfsVisit[y][x] + 1;
                    bfsQueue[queueTop] = board[y][x-1];
                    queueTop++;
                }
            }
            if(y < 18 && board[y+1][x].owner != null){
                if(bfsVisit[y+1][x] == -1) {
                    bfsVisit[y+1][x] = bfsVisit[y][x] + 1;
                    bfsQueue[queueTop] = board[y+1][x];
                    queueTop++;
                }
            }
            if(y > 0 && board[y-1][x].owner != null){
                if(bfsVisit[y-1][x] == -1) {
                    bfsVisit[y-1][x] = bfsVisit[y][x] + 1;
                    bfsQueue[queueTop] = board[y-1][x];
                    queueTop++;
                }
            }
        }

    }

    private int checkCounterAttack(Board attacker, Board object){
        int atk_x = attacker.boardView.getId()%15, atk_y = attacker.boardView.getId()/15;
        int obj_x = object.boardView.getId()%15, obj_y = object.boardView.getId()/15;
        int range = attacker.monster.getAttackRange();
        switch(attacker.monster.getAttackType()){
            case 0:
                return 0;
            case 1:
                if(atk_x == obj_x && Math.abs(atk_y-obj_y) <= range)
                    return 1;
                else if(atk_y == obj_y && Math.abs(atk_x-obj_x) <= range)
                    return 1;
                break;
            case 2:
                if(Math.abs(atk_x- obj_x)+Math.abs(atk_y-obj_y) <= range)
                    return 1;
                break;
            default:
                return 0;
        }
        return 0;
    }

    private void attackJudge(Board attackerBoard, Board defenderBoard){
        Monster attacker = attackerBoard.monster;
        Monster defender = defenderBoard.monster;
        if(defender.getName().compareTo("Dragon_Heart") == 0){
            defender.setHP(defender.getHP() - 1);
            switch(defender.getHP())
            {
                case 3:
                    defenderBoard.boardView.setImageResource(R.drawable.hp3);
                    defenderBoard.monster.setId(R.drawable.hp3);
                    break;
                case 2:
                    defenderBoard.boardView.setImageResource(R.drawable.hp2);
                    defenderBoard.monster.setId(R.drawable.hp2);
                    break;
                case 1:
                    defenderBoard.boardView.setImageResource(R.drawable.hp1);
                    defenderBoard.monster.setId(R.drawable.hp1);
                    break;
                default:
                    break;
            }
            if(defender.getHP() == 0){
                AlertDialog.Builder win_message = new AlertDialog.Builder(MainActivity2.this);
                win_message.setTitle("Congratulation!!");
                win_message.setMessage("You win");
                win_message.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialoginterface, int i) {
                            }
                        });
                win_message.show();
            }
        }
        else{
            if(checkCounterAttack(defenderBoard, attackerBoard) == 1)
                attacker.setHP(attacker.getHP() - defender.getATK());
            defender.setHP(defender.getHP() - attacker.getATK());
            if(attacker.getHP() <= 0){
                attackerBoard.monster = null;
                attackerBoard.boardView.setImageResource(0);
                attackerBoard.boardView.setBackgroundResource(attackerBoard.owner.getTile());
            }
            if(defender.getHP() <= 0){
                defenderBoard.monster = null;
                defenderBoard.boardView.setImageResource(0);
                defenderBoard.boardView.setBackgroundResource(defenderBoard.owner.getTile());
            }
        }

    }

    private Board checkAttackPosition(Board attackTarget, int attackType, int attackRange){
        Board possibleBoard = null;
        int[][] vector;
        int same;
        int target_x = attackTarget.boardView.getId()%15;
        int target_y = attackTarget.boardView.getId()/15;
        int leastMove = 285, least_x = -1, least_y = -1;
        if(attackType == 0)
            return possibleBoard;
        else if(attackType == 1){
            vector = new int[][] {{0,1}, {0, -1}, {1, 0}, {-1, 0}};
            same = 1;
            for(int i = attackRange; i > 0; i--) {
                for(int n = 0; n < 4; n++) {
                    int x = target_x + i * vector[n][1];
                    int y = target_y + i * vector[n][0];
                    if (x <= 14 && x >= 0 && y <= 18 && y >= 0) {
                        if (bfsVisit[y][x] != -1 && bfsVisit[y][x] != 1000 && bfsVisit[y][x] <= leastMove
                                && (board[y][x].monster == null || bfsVisit[y][x] == 0)) {
                            if (bfsVisit[y][x] == leastMove) {
                                same ++;
                                if((int) (Math.random() * same) < 1) {
                                    least_x = x;
                                    least_y = y;
                                }
                            } else {
                                same = 1;
                                least_x = x;
                                least_y = y;
                            }
                            leastMove = bfsVisit[y][x];
                            possibleBoard = board[y][x];
                        }
                    }
                }
            }
        }
        else if(attackType == 2){
            vector = new int[][] {{1,1}, {1, -1}, {-1, 1}, {-1,-1}};
            same = 1;
            for(int i = attackRange; i >= 0; i--){
                for(int j = attackRange-i; j >= 0; j--){
                    if(j + i == 0)
                        break;
                    else
                        for(int n = 0; n < 4; n++){
                            int x = target_x + i * vector[n][1];
                            int y = target_y + j * vector[n][0];
                            if (x <= 14 && x >= 0 && y <= 18 && y >= 0) {
                                if (bfsVisit[y][x] != -1 && bfsVisit[y][x] != 1000 && bfsVisit[y][x] <= leastMove
                                        && (board[y][x].monster == null || bfsVisit[y][x] == 0)) {
                                    if (bfsVisit[y][x] == leastMove) {
                                        same ++;
                                        if((int) (Math.random() * same) < 1) {
                                            least_x = x;
                                            least_y = y;
                                        }
                                    } else {
                                        same = 1;
                                        least_x = x;
                                        least_y = y;
                                    }
                                    leastMove = bfsVisit[y][x];
                                    possibleBoard = board[y][x];
                                }
                            }
                        }
                }
            }
        }

        Log.i("log", "least_x, y = "+least_y+""+least_x);
        return possibleBoard;
    }

    private final class BoardTouchListener implements View.OnTouchListener {
        private Thread newThread;
        public boolean onTouch(final View view, MotionEvent motionEvent) {
            int er = motionEvent.getAction();
            Log.i("Log", "action "+er);

            ImageView image = (ImageView)view;
            Board sourceBoard = board[image.getId()/15][image.getId()%15];
            if(sourceBoard.monster == null)
                return false;
            if(sourceBoard.monster.getOwner() != players[players_turn])
                return false;

            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                Log.i("Log", "action_down");
                dragornot = 1;
                tmp2 = ClipData.newPlainText("", "");
                run_or_not = 1;
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        newThread = currentThread();
                        multiListenerThread = newThread;
                        multiListenerWaiting = 1;
                        Log.i("Log", "action into thread");
                        try {
                            sleep(DRAG_SURE_TIME);
                            Log.i("Log", "action sleep finish");
                            if ( dragornot == 1 ) {
                                Log.i("Log", "action = 3");
                                dragornot = 3;
                                for(int i = 0; i < 8; i++){
                                    if(view == select_layout_print[i])index2 = i;
                                }
                                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                                monsterDragView = (ImageView)view;
                                multiListenerWaiting = 0;
                                run_or_not = 0;
                                view.startDrag(tmp2, shadowBuilder, view, 0);
                            }
                            else if(dragornot != 3){
                                dragornot = 0;
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
                return false;
            }
            else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                if(multiListenerWaiting == 1){
                    Log.i("Log", "action_up");
                    multiListenerThread.interrupt();
                    multiListenerWaiting = 0;
                }
                dragornot = 2;
                return false;
            }
            else if(motionEvent.getAction() == MotionEvent.ACTION_CANCEL){
                if(dragornot != 3){
                    if(multiListenerWaiting == 1){
                        Log.i("Log", "action_cancel");
                        multiListenerThread.interrupt();
                        multiListenerWaiting = 0;
                    }
                    dragornot = 2;
                }
                return false;
            }
            else {
                return false;
            }
        }
    }

    private class MonsterShadowBuilder extends View.DragShadowBuilder {

        private Bitmap image; // image to draw as a drag shadow

        public MonsterShadowBuilder(ImageView _image) {
            super();
            BitmapDrawable drawable = (BitmapDrawable) _image.getDrawable();
            image = Bitmap.createScaledBitmap(drawable.getBitmap(), pixels, pixels, false);
        }

        public void onDrawShadow(Canvas canvas)
        {
            canvas.drawBitmap(image, 0, 0, null);
            image.recycle();
        }

        public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint)
        {
            shadowSize.x = image.getWidth();
            shadowSize.y = image.getHeight();
            shadowTouchPoint.x = shadowSize.x / 2;
            shadowTouchPoint.y = shadowSize.y / 2;
        }
    }

    private final class ClearListener implements View.OnTouchListener {
        private Thread newThread;
        public boolean onTouch(final View view, final MotionEvent motionEvent) {
            int er = motionEvent.getAction();
            Log.i("Log", "action "+er);
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                Log.i("Log", "action_down");
                dragornot = 1;
                tmp2 = ClipData.newPlainText("", "");

                Thread thread = new Thread() {
                    @Override
                    public void run() {
                    newThread = currentThread();
                    multiListenerThread = newThread;
                    multiListenerWaiting = 1;
                    Log.i("Log", "action into thread");
                    try {
                        sleep(DRAG_SURE_TIME);
                        Log.i("Log", "action sleep finish");
                        if ( dragornot == 1 ) {
                            Log.i("Log", "action = 3");
                            dragornot = 3;
                            for(int i = 0; i < 8; i++){
                                if(view == select_layout_print[i])index2 = i;
                            }
                            MonsterShadowBuilder shadowBuilder = new MonsterShadowBuilder(tmp);
                            multiListenerWaiting = 0;
                            tmp.startDrag(tmp2, shadowBuilder, tmp, 0);
                        }
                        else if(dragornot != 3){
                            dragornot = 0;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    }
                };
                thread.start();
                return true;
            }
            else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                if(multiListenerWaiting == 1){
                    Log.i("Log", "action_up");
                    multiListenerThread.interrupt();
                    multiListenerWaiting = 0;
                }
                dragornot = 2;
                return false;
            }
            else if(motionEvent.getAction() == MotionEvent.ACTION_CANCEL){
                if(dragornot != 3){
                    if(multiListenerWaiting == 1){
                        Log.i("Log", "action_cancel");
                        multiListenerThread.interrupt();
                        multiListenerWaiting = 0;
                    }
                    dragornot = 2;
                }
                return false;
            }
            else {
                return false;
            }
        }
    }

    class MyDragListener implements View.OnDragListener {

        private boolean checkValid(int id){
            int surround_check = 0;
            for(int i = 0; i < 6; i++){
                if(id%15+current_index[i][0] < 0 || id%15+current_index[i][0] > 14) return false;
                if(id/15+current_index[i][1] < 0 || id/15+current_index[i][1] > 18) return false;
                int temp = id + current_index[i][0]+current_index[i][1]*15;
                if(temp < 0 || temp > 284) return false;
                if(board[temp/15][temp%15].owner != null) return false;
                if((temp%15) - 1 >= 0)
                    if(board[temp/15][(temp%15) - 1].owner != null)
                        if(board[temp/15][(temp%15) - 1].owner.getID() == players_turn) surround_check = 1;
                if((temp%15) + 1 <= 14)
                    if(board[temp/15][(temp%15) + 1].owner != null)
                        if(board[temp/15][(temp%15) + 1].owner.getID() == players_turn) surround_check = 1;
                if(temp/15 - 1 >= 0)
                    if(board[temp/15 - 1][temp%15].owner != null)
                        if(board[temp/15 - 1][temp%15].owner.getID() == players_turn) surround_check = 1;
                if(temp/15 + 1 <= 18)
                    if(board[temp/15 + 1][temp%15].owner != null)
                        if(board[temp/15 + 1][temp%15].owner.getID() == players_turn) surround_check = 1;
            }
            if(surround_check == 0) return false;
            return true;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int id;
            boolean valid;
            ImageView image;
            tmp2 = ClipData.newPlainText("", "");
            Log.i("Log", "get into ondrag");
            if(process == 3) {
                while (true) {
                    //Log.i("Log", "get into while true");
                    if (dragornot == 1) {
                        break;
                        //continue;
                    } else if (dragornot == 3) {
                        select_layout2.setVisibility(View.GONE);
                        switch (event.getAction()) {
                            case DragEvent.ACTION_DRAG_STARTED:
                                // do nothing
                                break;
                            case DragEvent.ACTION_DRAG_ENTERED:
                                //v.setBackgroundColor(0);
                                image = (ImageView) v;
                                id = image.getId();
                                valid = checkValid(id);
                                current_index = open_index[index1][index2];
                                for (int i = 0; i < 6; i++) {
                                    int temp = id + current_index[i][0] + current_index[i][1] * 15;
                                    Log.i("Log", "temp= " + temp);
                                    if (temp >= 0 && temp <= 284)
                                        image = (ImageView) findViewById(temp);
                                    else continue;
                                    if (board[temp / 15][temp % 15].owner == null) {
                                        if (valid) image.setBackgroundResource(R.drawable.white);
                                        else image.setBackgroundResource(R.drawable.else_invalid);
                                    } else image.setBackgroundResource(R.drawable.invalid_tile);
                                }
                                break;
                            case DragEvent.ACTION_DRAG_EXITED:
                                image = (ImageView) v;
                                id = image.getId();
                                current_index = open_index[index1][index2];
                                for (int i = 0; i < 6; i++) {
                                    int temp = id + current_index[i][0] + current_index[i][1] * 15;
                                    if (temp >= 0 && temp <= 284)
                                        image = (ImageView) findViewById(temp);
                                    else continue;
                                    if(board[temp/15][temp%15].owner == null)image.setBackgroundResource(R.drawable.black_tile);
                                    else{
                                        if(board[temp / 15][temp % 15].monster == null)
                                            image.setBackgroundResource(board[temp / 15][temp % 15].owner.getTile());
                                        else image.setBackgroundResource(board[temp / 15][temp % 15].monster.getOwner().getOccupiedTile(board[temp / 15][temp % 15].owner.getID()));
                                    }
                                }
                                break;
                            case DragEvent.ACTION_DROP:
                                image = (ImageView) v;
                                id = image.getId();
                                valid = checkValid(id);
                                if (valid) {
                                    int monsterid = id + current_index[0][0] + current_index[0][1] * 15;
                                    board[monsterid / 15][monsterid % 15].monster = summoningMonster;
                                    image.setImageResource(summoningMonster.getId());
                                    current_index = open_index[index1][index2];
                                    for (int i = 0; i < 6; i++) {
                                        int temp = id + current_index[i][0] + current_index[i][1] * 15;
                                        image = (ImageView) findViewById(temp);
                                        if(i == 0)image.setBackgroundResource(players[players_turn].getOccupiedTile(players[players_turn].getID()));
                                        else image.setBackgroundResource(players[players_turn].getTile());
                                        board[temp / 15][temp % 15].owner = players[players_turn];
                                    }
                                    players[players_turn].getBack(using);
                                    players[players_turn].setRemainDice(players[players_turn].getRemainDice() - 1);
                                    buttons[1].setText("SUMMON");
                                    process = 4;
                                } else {
                                    current_index = open_index[index1][index2];
                                    for (int i = 0; i < 6; i++) {
                                        int temp = id + current_index[i][0] + current_index[i][1] * 15;
                                        if (temp >= 0 && temp <= 284)
                                            image = (ImageView) findViewById(temp);
                                        else continue;
                                        if (board[temp / 15][temp % 15].owner == null)
                                            image.setBackgroundResource(R.drawable.black_tile);
                                        else{
                                            if(board[temp / 15][temp % 15].monster == null)
                                                image.setBackgroundResource(board[temp / 15][temp % 15].owner.getTile());
                                            else image.setBackgroundResource(board[temp / 15][temp % 15].monster.getOwner().getOccupiedTile(board[temp / 15][temp % 15].owner.getID()));
                                        }
                                    }
                                buttons[1].setText("SUMMON");
                                process = 2;
                                }
                                //process = 4;
                                break;
                            case DragEvent.ACTION_DRAG_ENDED:
                                //process = 4;
                                //v.setBackgroundDrawable(normalShape);
                            default:
                                break;
                        }
                        return true;
                    } else {
                        break;
                    }
                }
                Log.i("Log", "get out of while true");
                return false;
            }
            if(process == 2 || process == 4){
                while (true) {
                    //Log.i("Log", "get into while true");
                    if (dragornot == 1) {
                        break;
                        //continue;
                    } else if (dragornot == 3) {
                        switch (event.getAction()) {
                            case DragEvent.ACTION_DRAG_STARTED:
                                if(run_or_not == 0) {
                                    for (int i = 0; i < 19; i++)
                                        for (int j = 0; j < 15; j++)
                                            bfsVisit[i][j] = -1;
                                    bfsQueue[0] = board[monsterDragView.getId() / 15][monsterDragView.getId() % 15];
                                    bfsVisit[monsterDragView.getId() / 15][monsterDragView.getId() % 15] = 0;
                                    queueTop = 1;
                                    queuePointer = 0;
                                    checkMoveable(bfsQueue[0].monster.getMobility()*players[players_turn].getPool(1), bfsQueue[0].monster.getFlyable());
                                    run_or_not = 1;
                                }
                                break;
                            case DragEvent.ACTION_DRAG_ENTERED:
                                break;
                            case DragEvent.ACTION_DRAG_EXITED:
                                break;
                            case DragEvent.ACTION_DROP:
                                // Dropped, reassign View to ViewGroup
                                setToOriginColor();
                                image = (ImageView) v;
                                id = monsterDragView.getId();
                                Board source = board[id / 15][id % 15];
                                id = image.getId();
                                Board destin = board[id / 15][id % 15];
                                Board attackPosition;

                                if (destin.monster != null) {
                                    if(source.monster.getattackTime() == 0){
                                        break;
                                    }
                                    if (destin.monster.getOwner() != source.monster.getOwner() && players[players_turn].getPool(2) >= 1) {
                                        attackPosition = checkAttackPosition(destin, source.monster.getAttackType(), source.monster.getAttackRange());
                                        if(source.monster.getAttackType() == 0 || attackPosition == null){
                                            break;
                                        }
                                        if(attackPosition != source) {
                                            redeem = (int)( bfsVisit[attackPosition.boardView.getId()/15][attackPosition.boardView.getId() % 15]/source.monster.getMobility());
                                            if(bfsVisit[attackPosition.boardView.getId()/15][attackPosition.boardView.getId() % 15] % source.monster.getMobility() != 0 ) redeem++;
                                            players[players_turn].takePool(1,redeem);
                                            redeem = 0;
                                            diceData[1].setText("" + players[players_turn].getPool(1));
                                            attackPosition.monster = source.monster;
                                            attackPosition.boardView.setImageResource(source.monster.getId());
                                            monsterDragView.setImageResource(android.R.color.transparent);
                                            monsterDragView.setBackgroundResource(source.owner.getTile());
                                            attackPosition.boardView.setBackgroundResource(source.monster.getOwner().getOccupiedTile(destin.owner.getID()));
                                            source.monster = null;
                                        }
                                        attackPosition.monster.setattackTime(attackPosition.monster.getattackTime() - 1);
                                        players[players_turn].takePool(2, 1);
                                        diceData[0].setText("" + players[players_turn].getPool(2));
                                        attackJudge(attackPosition, destin);
                                        history[0] = null;
                                        history[1] = null;
                                        redeem = 0;
                                    }
                                    break;
                                }

                                if(bfsVisit[id/15][id%15] == -1)
                                    break;
                                //if (destin.owner == null) break;
                                history[0] = source;
                                history[1] = destin;
                                redeem = (int) (bfsVisit[id / 15][id % 15] / source.monster.getMobility());
                                if(bfsVisit[id / 15][id % 15] % source.monster.getMobility() != 0 ) redeem++;
                                players[players_turn].takePool(1, redeem);
                                diceData[1].setText(""+players[players_turn].getPool(1));

                                destin.monster = source.monster;
                                destin.boardView.setBackgroundResource(destin.monster.getOwner().getOccupiedTile(destin.owner.getID()));
                                source.boardView.setBackgroundResource(source.owner.getTile());
                                image.setImageResource(source.monster.getId());
                                monsterDragView.setImageResource(android.R.color.transparent);
                                source.monster = null;
                                break;
                            case DragEvent.ACTION_DRAG_ENDED:
                            default:
                        }
                        return true;
                    } else {
                        break;
                    }
                }
            }
            return false;
        }
    }

    class monsterSelector0 implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            if(players[players_turn].getRemainDice() > 3) {
                temp_monId = v.getId();
                Log.i("Log", "selected " + v.getId());
                tmp = (ImageView) v;
                summoningMonster = tempDice[0].getMonster();
                for (int i = 0; i < 3; i++) {
                    dice_image[i].setClickable(false);
                    dice_info[i].setVisibility(View.GONE);
                    dice_tag[i][0].setVisibility(View.GONE);
                    dice_tag[i][1].setVisibility(View.GONE);
                }
                using = tempDice[0].getserialNumber();
                //players[players_turn].getBack(tempDice[0].getserialNumber());
                //players[players_turn].setRemainDice(players[players_turn].getRemainDice() - 1);
                dice_select_layout.setVisibility(View.GONE);
                select_layout.setVisibility(View.VISIBLE);
                process = 3;
            }
        }
    }
    class monsterSelector1 implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            if(players[players_turn].getRemainDice() > 3) {
                temp_monId = v.getId();
                Log.i("Log", "selected " + v.getId());
                tmp = (ImageView) v;
                summoningMonster = tempDice[1].getMonster();
                for (int i = 0; i < 3; i++) {
                    dice_image[i].setClickable(false);
                    dice_info[i].setVisibility(View.GONE);
                    dice_tag[i][0].setVisibility(View.GONE);
                    dice_tag[i][1].setVisibility(View.GONE);
                }
                using = tempDice[1].getserialNumber();
                //players[players_turn].getBack(tempDice[1].getserialNumber());
                //players[players_turn].setRemainDice(players[players_turn].getRemainDice() - 1);
                dice_select_layout.setVisibility(View.GONE);
                select_layout.setVisibility(View.VISIBLE);
                process = 3;
            }
        }
    }
    class monsterSelector2 implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            if(players[players_turn].getRemainDice() > 3) {
                temp_monId = v.getId();
                Log.i("Log", "selected " + v.getId());
                tmp = (ImageView) v;
                summoningMonster = tempDice[2].getMonster();
                for (int i = 0; i < 3; i++) {
                    dice_image[i].setClickable(false);
                    dice_info[i].setVisibility(View.GONE);
                    dice_tag[i][0].setVisibility(View.GONE);
                    dice_tag[i][1].setVisibility(View.GONE);
                }
                using = tempDice[2].getserialNumber();
                //players[players_turn].getBack(tempDice[2].getserialNumber());
                //players[players_turn].setRemainDice(players[players_turn].getRemainDice() - 1);
                select_layout.setVisibility(View.VISIBLE);
                dice_select_layout.setVisibility(View.GONE);
                process = 3;
            }
        }
    }

    public void ChangeImage(int i){
        select_layout_print[4].setVisibility(View.VISIBLE);
        select_layout_print[5].setVisibility(View.VISIBLE);
        select_layout_print[6].setVisibility(View.VISIBLE);
        select_layout_print[7].setVisibility(View.VISIBLE);
        index1 = i-1;
        if(i == 1) {
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_1_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_1_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_1_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_1_4, 100, 100));
            select_layout_print[4].setVisibility(View.GONE);
            select_layout_print[5].setVisibility(View.GONE);
            select_layout_print[6].setVisibility(View.GONE);
            select_layout_print[7].setVisibility(View.GONE);
        }else if(i == 2){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_2_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_2_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_2_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_2_4, 100, 100));
            select_layout_print[4].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_2_5, 100, 100));
            select_layout_print[5].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_2_6, 100, 100));
            select_layout_print[6].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_2_7, 100, 100));
            select_layout_print[7].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_2_8, 100, 100));
        }else if(i == 3){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_3_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_3_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_3_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_3_4, 100, 100));
            select_layout_print[4].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_3_5, 100, 100));
            select_layout_print[5].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_3_6, 100, 100));
            select_layout_print[6].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_3_7, 100, 100));
            select_layout_print[7].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_3_8, 100, 100));
        }else if(i == 4){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_4_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_4_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_4_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_4_4, 100, 100));
            select_layout_print[4].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_4_5, 100, 100));
            select_layout_print[5].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_4_6, 100, 100));
            select_layout_print[6].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_4_7, 100, 100));
            select_layout_print[7].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_4_8, 100, 100));
        }else if(i == 5){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_5_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_5_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_5_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_5_4, 100, 100));
            select_layout_print[4].setVisibility(View.GONE);
            select_layout_print[5].setVisibility(View.GONE);
            select_layout_print[6].setVisibility(View.GONE);
            select_layout_print[7].setVisibility(View.GONE);
        }else if(i == 6){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_6_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_6_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_6_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_6_4, 100, 100));
            select_layout_print[4].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_6_5, 100, 100));
            select_layout_print[5].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_6_6, 100, 100));
            select_layout_print[6].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_6_7, 100, 100));
            select_layout_print[7].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_6_8, 100, 100));
        }else if(i == 7){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_7_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_7_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_7_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_7_4, 100, 100));
            select_layout_print[4].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_7_5, 100, 100));
            select_layout_print[5].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_7_6, 100, 100));
            select_layout_print[6].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_7_7, 100, 100));
            select_layout_print[7].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_7_8, 100, 100));
        }else if(i == 8){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_8_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_8_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_8_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_8_4, 100, 100));
            select_layout_print[4].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_8_5, 100, 100));
            select_layout_print[5].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_8_6, 100, 100));
            select_layout_print[6].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_8_7, 100, 100));
            select_layout_print[7].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_8_8, 100, 100));
        }else if(i == 9){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_9_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_9_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_9_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_9_4, 100, 100));
            select_layout_print[4].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_9_5, 100, 100));
            select_layout_print[5].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_9_6, 100, 100));
            select_layout_print[6].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_9_7, 100, 100));
            select_layout_print[7].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_9_8, 100, 100));
        }else if(i == 10){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_10_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_10_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_10_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_10_4, 100, 100));
            select_layout_print[4].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_10_5, 100, 100));
            select_layout_print[5].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_10_6, 100, 100));
            select_layout_print[6].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_10_7, 100, 100));
            select_layout_print[7].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_10_8, 100, 100));
        }else if(i == 11){
            select_layout_print[0].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_11_1, 100, 100));
            select_layout_print[1].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_11_2, 100, 100));
            select_layout_print[2].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_11_3, 100, 100));
            select_layout_print[3].setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.dice_open_11_4, 100, 100));
            select_layout_print[4].setVisibility(View.GONE);
            select_layout_print[5].setVisibility(View.GONE);
            select_layout_print[6].setVisibility(View.GONE);
            select_layout_print[7].setVisibility(View.GONE);
        }
    }

    public void Initial(){

        //Initial Players
        players[0] = new Player(0, R.drawable.hp3);
        players[1] = new Player(1, R.drawable.hp3);

        for(int i = 0; i < 19; i++){
            for(int j = 0; j < 15; j++)board[i][j] = new Board(null);
        }

        //Initial Players
        players[0].setTile(R.drawable.shape_p1, R.drawable.p1_occupied, R.drawable.p1_occupiedp2);
        players[1].setTile(R.drawable.shape_p2, R.drawable.p2_occupied, R.drawable.p2_occupiedp1);


        GridLayout container = (GridLayout)findViewById(R.id.container);
        multiListenerWaiting = 0;
        multiListenerThread = null;

        ImageView tempImage = new ImageView(this);
        scale = tempImage.getContext().getResources().getDisplayMetrics().density;
        pixels = (int) (50 * scale + 0.5f);

        for(int i = 0; i < 19; i++){
            for(int j = 0; j < 15; j++){
                tempImage = new ImageView(this);
                tempImage.setLayoutParams(new GridView.LayoutParams(pixels, pixels));
                tempImage.setScaleType(ImageView.ScaleType.FIT_XY);
                tempImage.setPadding(0, 0, 0, 0);
                tempImage.setBackgroundResource(R.drawable.black_tile);
                tempImage.setId(i * 15 + j);
                tempImage.setOnDragListener(new MyDragListener());
                tempImage.setOnTouchListener(new BoardTouchListener());
                tempImage.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(board[v.getId()/15][v.getId()%15].monster != null){
                            Toast.makeText(MainActivity2.this, board[v.getId()/15][v.getId()%15].monster.getName() + "\n" + "HP : " + board[v.getId()/15][v.getId()%15].monster.getHP() + "\n" + "ATK : " + board[v.getId()/15][v.getId()%15].monster.getATK(), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(MainActivity2.this, "" + v.getId(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                board[i][j].boardView = tempImage;
                container.addView(tempImage);
            }
        }

        board[0][7].boardView.setImageResource(R.drawable.hp3);
        board[0][7].owner = players[0];
        board[0][7].monster = players[0].getDragon_heart();
        board[18][7].boardView.setImageResource(R.drawable.hp3);
        board[18][7].owner = players[1];
        board[18][7].monster = players[1].getDragon_heart();

        history[0] = null;
        history[1] = null;
        redeem = 0;

        for(int i = 0; i < 8; i++){
            select_layout_print[i].setOnTouchListener(new ClearListener());
        }

        dice_image[0].setOnClickListener(new monsterSelector0());
        dice_image[1].setOnClickListener(new monsterSelector1());
        dice_image[2].setOnClickListener(new monsterSelector2());
        for(int i = 0; i < 3; i++) {
            dice_image[i].setClickable(false);
            dice_info[i].setBackgroundResource(R.drawable.white);
            dice_info[i].setVisibility(View.GONE);
            dice_info[i].setMovementMethod(new ScrollingMovementMethod());
        }
        dice_tag[0][0].setImageResource(R.drawable.wing);
        dice_tag[1][0].setImageResource(R.drawable.wing);
        dice_tag[2][0].setImageResource(R.drawable.wing);
        for(int i = 0; i < 3; i++)
            for(int j = 0 ; j < 2; j++) dice_tag[i][j].setVisibility(View.GONE);

        buttons[0].setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(process == 1) {
                    dice_select_layout.setVisibility(View.VISIBLE);
                    tempDice = players[players_turn].throwDice();

                    for (int i = 0; i < 3; i++){
                        dice_faces[i] = tempDice[i].getFace((int) (Math.random() * 6));
                        Log.i("Log", "dice" + i + " result: " + dice_faces[i]);
                        switch(dice_faces[i]){
                            case 0:
                                dice_image[i].setImageResource(R.drawable.asd);
                                break;
                            case 1:
                                dice_image[i].setImageResource(R.drawable.move);
                                break;
                            case 2:
                                dice_image[i].setImageResource(R.drawable.attack);
                                break;
                            case 3:
                                dice_image[i].setImageResource(R.drawable.asd);
                                break;
                            case 4:
                                dice_image[i].setImageResource(R.drawable.asd);
                                break;
                            case 5:
                                dice_image[i].setImageResource(R.drawable.asd);
                                break;
                            default:
                                Log.e("Log", "dice error");
                                break;
                        }
                        players[players_turn].addPool(dice_faces[i]);
                        //attack
                        diceData[0].setText("" + players[players_turn].getPool(2));
                        //move
                        diceData[1].setText("" + players[players_turn].getPool(1));
                    }
                    if(players[players_turn].getPool(0) >= 2) {
                        process = 2;
                    }
                    else {
                        process = 4;
                    }
                }
            }
        });

        buttons[1].setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((process == 2 && players[players_turn].getRemainDice() > 3) || (process == 3 && select_layout.getVisibility() == View.VISIBLE)) {
                    Log.i("Log", "reset images");
                    select_layout.setVisibility(View.GONE);
                    dice_select_layout.setVisibility(View.VISIBLE);
                    int temp_id;
                    for(int i = 0; i < 3; i++){
                        temp_id = tempDice[i].getMonster().getId();
                        if(dice_faces[i] != 0){
                            dice_image[i].setImageResource(R.drawable.closed);
                            continue;
                        }
                        dice_image[i].setImageResource(temp_id);
                        dice_image[i].setBackgroundResource(R.drawable.white);
                        dice_image[i].setClickable(true);
                        if(tempDice[i].getMonster().getFlyable())dice_tag[i][0].setVisibility(View.VISIBLE);
                        switch(tempDice[i].getMonster().getAttackType()){
                            case 0:
                                dice_tag[i][1].setImageResource(R.drawable.atk_type0);
                                break;
                            case 1:
                                dice_tag[i][1].setImageResource(R.drawable.atk_type1);
                                break;
                            case 2:
                                dice_tag[i][1].setImageResource(R.drawable.atk_type2);
                                break;
                            default:
                                Log.e("Log", "get attack type error");
                                break;
                        }
                        dice_tag[i][1].setVisibility(View.VISIBLE);
                        dice_info[i].setVisibility(View.VISIBLE);
                        dice_info[i].setText(""+tempDice[i].getMonster().getName()+
                                             "\nHP: "+tempDice[i].getMonster().getHP()+
                                             //"\nfly: "+tempDice[i].getMonster().getFlyable()+
                                             "\nMobility: "+tempDice[i].getMonster().getMobility()+
                                             "\nAtk: "+tempDice[i].getMonster().getATK()+
                                             //"\nAtkType: "+tempDice[i].getMonster().getAttackType()+
                                             "\nAtkRange: "+tempDice[i].getMonster().getAttackRange()+
                                             "\nAtkTimes: "+tempDice[i].getMonster().getMaxattackTime());
                    }
                    buttons[1].setText("BACK");
                    process = 3;
                }
                else if(process == 3){
                    if(dice_select_layout.getVisibility() == View.VISIBLE){
                        buttons[1].setText("SUMMON");
                        dice_select_layout.setVisibility(View.GONE);
                        process = 2;
                    }
                    else if(select_layout2.getVisibility() == View.VISIBLE){
                        select_layout2.setVisibility(View.GONE);
                        select_layout.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        buttons[2].setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(process >= 2) {
                    select_layout.setVisibility(View.GONE);
                    select_layout2.setVisibility(View.GONE);
                    dice_select_layout.setVisibility(View.GONE);
                    buttons[1].setText("SUMMON");


                    tempDice = null;
                    temp_monId = -1;
                    players[players_turn].clearPool();

                    if (players_turn == 0) {
                        players_turn = 1;
                        buttons[2].setText("END(P2)");
                    } else {
                        players_turn = 0;
                        buttons[2].setText("END(P1)");
                    }

                    diceData[0].setText("" + players[players_turn].getPool(2));
                    diceData[1].setText("" + players[players_turn].getPool(1));
                    for(int i = 0; i < 3; i++){
                        dice_image[i].setImageResource(R.drawable.white);
                        dice_info[i].setVisibility(View.GONE);
                        for(int j = 0; j < 2; j++)dice_tag[i][j].setVisibility(View.GONE);
                    }
                    process = 1;

                    for(int i = 0 ;i < 285;i++)
                    {
                        if(board[i/15][i%15].monster != null)
                        {
                            board[i/15][i%15].monster.setattackTime(board[i/15][i%15].monster.getMaxattackTime());
                        }
                    }
                }
            }
        });

        buttons[3].setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(process == 2 || process == 4){
                    if(history[0] != null && history[1] != null && redeem > 0){
                        history[0].monster = history[1].monster;
                        history[0].boardView.setBackgroundResource(history[0].monster.getOwner().getOccupiedTile(history[0].owner.getID()));
                        history[1].boardView.setBackgroundResource(history[1].owner.getTile());
                        history[0].boardView.setImageResource(history[0].monster.getId());
                        history[1].boardView.setImageResource(android.R.color.transparent);
                        history[1].monster = null;

                        while(redeem > 0){
                            players[players_turn].addPool(1);
                            redeem--;
                        }
                        //move
                        diceData[1].setText("" + players[players_turn].getPool(1));

                        history[0] = null;
                        history[1] = null;
                        redeem = 0;
                    }
                    else{
                        dice_select_layout.setVisibility(View.GONE);
                        for(int i = 0; i < 3; i++){
                            dice_info[i].setVisibility(View.GONE);
                            for(int j = 0; j < 2; j++) dice_tag[i][j].setVisibility(View.GONE);
                        }
                    }
                }
            }
        });

        int i;
        for(i = 0; i < 11; i++){
            final int temp = i;
            select[temp].setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    select_layout.setVisibility(View.GONE);
                    select_layout2.setVisibility(View.VISIBLE);
                    ChangeImage(temp+1);
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        set_dice_open_index();

        select_layout = (GridLayout) findViewById(R.id.Grid);
        select_layout.setVisibility(View.GONE);
        select_layout2 = (GridLayout) findViewById(R.id.select_layout2_1);
        select_layout2.setVisibility(View.GONE);
        dice_select_layout = (LinearLayout) findViewById(R.id.diceLayout);
        dice_select_layout.setVisibility(View.GONE);
        dice_image[0] = (ImageView)findViewById(R.id.diceImage1);
        dice_image[1] = (ImageView)findViewById(R.id.diceImage2);
        dice_image[2] = (ImageView)findViewById(R.id.diceImage3);
        dice_info[0] = (TextView)findViewById(R.id.diceInfo1);
        dice_info[1] = (TextView)findViewById(R.id.diceInfo2);
        dice_info[2] = (TextView)findViewById(R.id.diceInfo3);
        dice_tag[0][0] = (ImageView)findViewById(R.id.diceTag1_1);
        dice_tag[0][1] = (ImageView)findViewById(R.id.diceTag1_2);
        dice_tag[1][0] = (ImageView)findViewById(R.id.diceTag2_1);
        dice_tag[1][1] = (ImageView)findViewById(R.id.diceTag2_2);
        dice_tag[2][0] = (ImageView)findViewById(R.id.diceTag3_1);
        dice_tag[2][1] = (ImageView)findViewById(R.id.diceTag3_2);

        buttons[0] = (Button)findViewById(R.id.button1);
        buttons[1] = (Button)findViewById(R.id.button2);
        buttons[2] = (Button)findViewById(R.id.button3);
        buttons[3] = (Button)findViewById(R.id.button4);
        diceData[0] = (TextView)findViewById(R.id.textView1);
        diceData[1] = (TextView)findViewById(R.id.textView2);

        select[0] = (ImageButton)findViewById(R.id.select1);
        select[1] = (ImageButton)findViewById(R.id.select2);
        select[2] = (ImageButton)findViewById(R.id.select3);
        select[3] = (ImageButton)findViewById(R.id.select4);
        select[4] = (ImageButton)findViewById(R.id.select5);
        select[5] = (ImageButton)findViewById(R.id.select6);
        select[6] = (ImageButton)findViewById(R.id.select7);
        select[7] = (ImageButton)findViewById(R.id.select8);
        select[8] = (ImageButton)findViewById(R.id.select9);
        select[9] = (ImageButton)findViewById(R.id.select10);
        select[10] = (ImageButton)findViewById(R.id.select11);
        select_layout_print[0] = (ImageView)findViewById(R.id.select_1);
        select_layout_print[1] = (ImageView)findViewById(R.id.select_2);
        select_layout_print[2] = (ImageView)findViewById(R.id.select_3);
        select_layout_print[3] = (ImageView)findViewById(R.id.select_4);
        select_layout_print[4] = (ImageView)findViewById(R.id.select_5);
        select_layout_print[5] = (ImageView)findViewById(R.id.select_6);
        select_layout_print[6] = (ImageView)findViewById(R.id.select_7);
        select_layout_print[7] = (ImageView)findViewById(R.id.select_8);

        select_layout = (GridLayout) findViewById(R.id.Grid);
        select_layout2 = (GridLayout) findViewById(R.id.select_layout2_1);

        Initial();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
