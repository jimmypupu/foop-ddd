# Dungeon Dice Dragon - D.D.D. #


### Recent progress ###

* Ver.beta.1.0
* Can normally end game

### How to play? ###

* Install in android phone by apk file.
* At the beginning of a turn, player must click "throw" first to throw three dices.
* If there are more than 2 dices with summon faces, player can choose to click "summon" to summon.
* When summoning monster, player need to choose which dice to open, and then choose how to open and drag to the board.
* Except for summoning, every period of the game you can try to drag your summoned monster on the board.
* When player want to end a turn, click "end" button.

### Rule of D.D.D. ###

* Player's goal: tries to defeat another player's dragon heart!
* Throw dices with 6 faces: attack, defense, summon, magic, move and trap!!
* With two summon faces, player can try to summon a monster in the dice, opening the dice on the board!

### Contributor ###

* National Taiwan University, Computer science and information engineering
* B01902014 丁柏文
* B01902016 嚴培軒
* B01902098 黃俊嘉
* B01902108 林碩彥 